const
    gulp          = require('gulp'),
    htmlmin       = require('gulp-htmlmin'),
    imagemin      = require('gulp-imagemin'),
    imgCompress   = require('imagemin-jpeg-recompress'),
    cleanCSS      = require('gulp-clean-css'),
    del           = require('del'),
    stylus        = require('gulp-stylus'),
    autoprefixer  = require('gulp-autoprefixer'),
    browserSync   = require('browser-sync').create();

gulp.task('del', () =>
    del(['dist/**', '!dist'])
);

gulp.task('styles', function() {
  return gulp.src('src/stylus/main.styl')
      .pipe(stylus())
      .pipe(autoprefixer(['last 2 versions', '> 2%', 'ie 11'], { cascade: true }))  // добавляем вендорные префиксы
      .pipe(gulp.dest('src/css'))
      .pipe(browserSync.reload({stream: true}))
});

gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
      baseDir: 'src'
    },
    notify: false
  });
  browserSync.watch('src', browserSync.reload)
});

gulp.task('minify-css', () =>
    gulp.src('./src/css/*.css')
        // .pipe(autoprefixer(['last 2 versions', '> 1%', 'ie 11'], { cascade: true }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('dist/css'))
)

gulp.task('move-js', () =>
    gulp.src('./src/js/*.js')
        .pipe(gulp.dest('dist/js'))
)

gulp.task('htmlmin', () =>
    gulp.src('./src/*.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('dist'))
)

gulp.task('move-fonts', () =>
    gulp.src('./src/fonts/**/*')
        .pipe(gulp.dest('dist/fonts'))
)

gulp.task('imgmin', () => {
  return gulp.src('src/img/**/*')
      .pipe(imagemin([
        imgCompress({
          loops: 4,
          min: 70,
          max: 80,
          quality: 'high'
        }),
        imagemin.gifsicle(),
        imagemin.optipng(),
        imagemin.svgo()
      ]))
      .pipe(gulp.dest('dist/img'));
});

gulp.task('watch', function(){
  gulp.watch('src/stylus/**/*.styl', gulp.series('styles'))
});

gulp.task('default', gulp.series(
    // gulp.series('styles'),
    gulp.parallel('watch', 'browser-sync')
))

gulp.task('build', gulp.series('del', 'styles', 'minify-css', 'move-js', 'htmlmin', 'move-fonts', 'imgmin', function(done){
  done();
}))