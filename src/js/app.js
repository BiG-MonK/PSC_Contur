let elSlides = document.querySelectorAll('.header__slide'),
    elBullets = document.querySelectorAll('.header__bullets-item'),
    sliderCounter = 1,   // Просто технический счетчик для авто-смены слайдера
    interval,
    slidesLength = elSlides.length,
// Переменные которые можно менять подбирая
// ----- Анимация печати текста ------
    beforeAnimationPrint = 1000, // ms   задержка перед самой анимации печати
    printCharTime = 120,         // ms   время печати одного символа
    delCharTime = 70,            // ms   время удаления одного символа
    beforeDelChar = 1000,        // ms   задержка перед удалением первого слова
    beforePrintNextWord = 100,   // ms   задержка перед печатанием следующего слова
// ----- Анимация слайдера ------
    nextSlideTime = 5000,        // ms   время смены слайдера
// ----- Анимация счетчика числа в блоке отчеты ------
    countNumberTime = 35         // ms   задержка перед суммирование следующего шага числа
// ---------------------------------------------------------------------------

// ------------------------ Слайдер в блоке header --------------------------
elSlides[0].classList.add('header__slide--active-main')
function forceUpdateTimer() {
    clearInterval(interval)
    interval = setInterval(function() {
        elSlides.forEach((el, i) => {
            if (el.classList.contains('header__slide--active-main')) {
                el.classList.remove('header__slide--active-main')
                el.classList.add('header__slide--active-bg')
                setTimeout(() => {
                    el.classList.remove('header__slide--active-bg')
                }, 800)
                sliderCounter = i
            }
        })
        elSlides[(sliderCounter + 1) % slidesLength].classList.add('header__slide--active-main')
        nextActivated(elBullets, 'header__bullets-item--active', elBullets[(sliderCounter + 1) % elBullets.length])
        sliderCounter ++
    }, nextSlideTime);
}
forceUpdateTimer()

// Функция активации элемента из списка элементов
function nextActivated (arr, findClass, elem) {
    arr.forEach((el, i) => {
        if (el.classList.contains(findClass)) {
            el.classList.remove(findClass)
        }
    })
    elem.classList.add(findClass)
}

let clickTime = 0
// Обработчик события на клик мыши в кнопках слайдера блока Header
elBullets.forEach((bullet, i) => bullet.onclick = () => {
    if (bullet.classList.contains('header__bullets-item--active')) return 1 // Если клик по уже активной кнопке,то ничего не делать
    if ((new Date().getTime() - clickTime) < 1000) return 1 // Если время между кликами менее минуты,то ничего не делать
    clickTime = new Date().getTime()

    setTimeout(() => {
        sliderCounter = i
        if (!bullet.classList.contains('header__bullets-item--active')) {
            nextActivated(elBullets, 'header__bullets-item--active', bullet)
        }
        else return 1
        elSlides.forEach((el, i) => {
            if (el.classList.contains('header__slide--active-main')) {
                el.classList.remove('header__slide--active-main')
                el.classList.add('header__slide--active-bg')
                setTimeout(() => {
                    el.classList.remove('header__slide--active-bg')
                }, 800)
            }
        })
        nextActivated(elSlides, 'header__slide--active-main', elSlides[sliderCounter % slidesLength])

        forceUpdateTimer()
    },500)

})

// ------------------------ Обработка плавного перехода по якорям --------------------------
let pageLinks = document.querySelectorAll('.nav__item a')
function scrollToSection(e) {
    let targetSection = document.getElementById(e.dataset.targetSection)
    targetSection.scrollIntoView({ behavior: 'smooth' })
}
pageLinks.forEach( element => {
    element.addEventListener('click', (event) => {
        event.preventDefault()
        scrollToSection(element)
    }, false)
})

// ------------------------ Анимация печати заголовка сайта --------------------------
let str_1 = 'КОНТУР',
    str_2 = 'Частное',
    str_3 = 'Честное охранное предприятие',
    elTitle = document.querySelector('.header__running-text-js'),
    elTitle2 = document.querySelector('.header__running-text2-js'),
    timePrintSecondString = beforeAnimationPrint + str_1.length * printCharTime + beforeDelChar + str_1.length * delCharTime + beforePrintNextWord,
    timePrintThirdString = timePrintSecondString + str_2.length * printCharTime + beforeDelChar + str_2.length * delCharTime + beforePrintNextWord,
    timePrintThirdString_2 = timePrintThirdString + str_3.length * printCharTime + beforePrintNextWord

setTimeout(() => print_el_text(elTitle, str_1), beforeAnimationPrint)
setTimeout(() => print_el_text(elTitle, str_2), timePrintSecondString)
setTimeout(() => print_el_text(elTitle, str_3), timePrintThirdString)
setTimeout(() => print_el_text(elTitle2, str_1), timePrintThirdString_2)

// Функция визуализации печати строки
function print_el_text(el, text) {
    let i = 0
    let __print = function () {
        i++
        if ( i <= text.length ) {
            if (i === text.length) el.innerHTML = text.substr(0, i)
            else el.innerHTML = text.substr(0, i);
            setTimeout( __print, printCharTime )
        } else {
            if (((text === str_1) || (text === str_2)) && (el === elTitle)) {
                setTimeout(() => delete_el_text(elTitle), beforeDelChar)
            }
        }
    }
    __print()
}
// Функция визуализации удаления строки
function delete_el_text(el) {
    let text = el.innerHTML
    let i = text.length
    let __delete = function () {
        i--
        if ( i >= 0 ) {
            if (i === 0) el.innerHTML = '&nbsp'
            else el.innerHTML = text.substr(0, i)
            setTimeout( __delete, delCharTime )
        }
    }
    __delete()
}

// ------------------------ Анимация счетчика цифр в блоке отчеты --------------------------
let elReportObjects = document.querySelector('.report__objects-js'),
    elReportYears = document.querySelector('.report__year-js'),
    elReportClients = document.querySelector('.report__clients-js')
const options = {
        root: null,
        rootMargin: '0px',
        threshold: 0.5},
      elReportWrap = document.querySelector('.report')
function animateCountNumber(el, startNum, finishNum, step) {
    let __print = function () {
        startNum += step
        if ( startNum <= finishNum ) {
            el.innerHTML = startNum
            setTimeout( __print, countNumberTime)
        } else el.innerHTML += '+'
    }
    __print()
}
function handleReport(report) {
    report.forEach(rep => {
        if (rep.intersectionRatio > 0) {
            animateCountNumber(elReportObjects, 0, 90, 5)
            animateCountNumber(elReportYears, 0, 20, 2)
            animateCountNumber(elReportClients, 0, 60, 3)
        }
    })
}
const observerReport = new IntersectionObserver(handleReport, options)
observerReport.observe(elReportWrap)

// ------------------------ Двигающийся текст в блоке преимущества --------------------------
let movingText = document.querySelector('.moving-string__text-js')
let sectionOfMovingText = document.querySelector('.advantages')
let curElemPos,
    curSecPos,
    perView

document.addEventListener('scroll', function(e) {
    curElemPos = movingText.getBoundingClientRect()
    curSecPos = sectionOfMovingText.getBoundingClientRect()
    if ((curSecPos.top < window.innerHeight) && (curSecPos.top > 0)) { // Если вся секция уже видна и ее вверх не дошел до начала экрана
        perView = ((window.innerHeight - curSecPos.top) * 100) / window.innerHeight // путь движения секции с низу вверх в %
        movingText.style.right = 100 - perView + '%'
        movingText.style.opacity = (perView / 100) - 0.45 + ''
    }
    if (curSecPos.top < 0) {
        movingText.style.right = 0 + '%'
    }
})

// ------------------------ 3D эффект при наведении в блоке тарифов --------------------------
let services = document.querySelectorAll('.service')
services.forEach((service) => {
    service.addEventListener('mousemove', startRotate)
    service.addEventListener('mouseout', stopRotate)

})
function startRotate(event) {
    let service = this.querySelector('.service__wrapper-interior')
    let halfHeight = service.offsetHeight / 2
    let halfWidth = service.offsetWidth / 2
    service.style.transition = 'transform 0.2s ease'
    service.style.transform = 'rotateX(' + -(event.offsetY - halfHeight) / 8 + 'deg) rotateY('
        + (event.offsetX - halfWidth) / 10 + 'deg)'
}
function stopRotate() {
    let service = this.querySelector('.service__wrapper-interior')
    service.style.transform = 'rotate(0)'
    service.style.transition = 'transform 0.8s ease'
}

//----------------------------------------------------------------
let lastQuestion = document.querySelector('.item:last-of-type')
let lastButOneQuestion = document.querySelector('.item:nth-last-child(2)')
let lastButOneQuestion2 = document.querySelector('.item:nth-last-child(3)')



lastQuestion.addEventListener('mouseenter', () => {
    // lastButOneQuestion.classList.add('.custom-accordion__item--active')
    setTimeout(function(){
        lastButOneQuestion.style.opacity = '0.1'
        lastButOneQuestion.style.zIndex = '11'
    },150);

})
lastQuestion.addEventListener('mouseleave', () => {
    // lastButOneQuestion.classList.remove('.custom-accordion__item--active')
    setTimeout(function(){
        lastButOneQuestion.style.opacity = '1'
        lastButOneQuestion.style.zIndex = '10'
    },150);
})
lastButOneQuestion.addEventListener('mouseenter', () => {
    // lastQuestion.style.opacity = '0.1'
    setTimeout(function(){
        lastQuestion.style.zIndex = '11'
    },200);
    // lastQuestion.style.zIndex = '11'
})
lastButOneQuestion.addEventListener('mouseleave', () => {
    // lastQuestion.style.opacity = '1'
    setTimeout(function(){
        lastQuestion.style.zIndex = '10'
    },200);
    // lastQuestion.style.zIndex = '10'
})
lastButOneQuestion2.addEventListener('mouseenter', () => {
    lastButOneQuestion.style.opacity = '0.1'
//     // lastButOneQuestion.style.zIndex = '11'
})
lastButOneQuestion2.addEventListener('mouseleave', () => {
    lastButOneQuestion.style.opacity = '1'
//     // lastButOneQuestion.style.zIndex = '10'
})
console.log(lastButOneQuestion)







